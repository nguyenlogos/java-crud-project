package com.example.model;
// Generated Jul 9, 2019 10:29:51 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

/**
 * CommDt generated by hbm2java
 */
public class CommDt implements java.io.Serializable {

	private Integer cdid;
	private String mtCd;
	private String dtCd;
	private String dtNm;
	private String dtKr;
	private String dtVn;
	private String dtExp;
	private String upCd;
	private String val1;
	private String val1Nm;
	private String val2;
	private String val2Nm;
	private String val3;
	private String val3Nm;
	private String val4;
	private String val4Nm;
	private Integer dtOrder;
	private Character useYn;
	private Character delYn;
	private String regId;
	private Date regDt;
	private String chgId;
	private Date chgDt;
	private String unit;

	public CommDt() {
	}

	public CommDt(String mtCd, String dtCd, String dtNm, Date regDt, Date chgDt) {
		this.mtCd = mtCd;
		this.dtCd = dtCd;
		this.dtNm = dtNm;
		this.regDt = regDt;
		this.chgDt = chgDt;
	}

	public CommDt(String mtCd, String dtCd, String dtNm, String dtKr, String dtVn, String dtExp, String upCd,
			String val1, String val1Nm, String val2, String val2Nm, String val3, String val3Nm, String val4,
			String val4Nm, Integer dtOrder, Character useYn, Character delYn, String regId, Date regDt, String chgId,
			Date chgDt, String unit) {
		this.mtCd = mtCd;
		this.dtCd = dtCd;
		this.dtNm = dtNm;
		this.dtKr = dtKr;
		this.dtVn = dtVn;
		this.dtExp = dtExp;
		this.upCd = upCd;
		this.val1 = val1;
		this.val1Nm = val1Nm;
		this.val2 = val2;
		this.val2Nm = val2Nm;
		this.val3 = val3;
		this.val3Nm = val3Nm;
		this.val4 = val4;
		this.val4Nm = val4Nm;
		this.dtOrder = dtOrder;
		this.useYn = useYn;
		this.delYn = delYn;
		this.regId = regId;
		this.regDt = regDt;
		this.chgId = chgId;
		this.chgDt = chgDt;
		this.unit = unit;
	}

	public Integer getCdid() {
		return this.cdid;
	}

	public void setCdid(Integer cdid) {
		this.cdid = cdid;
	}

	public String getMtCd() {
		return this.mtCd;
	}

	public void setMtCd(String mtCd) {
		this.mtCd = mtCd;
	}

	public String getDtCd() {
		return this.dtCd;
	}

	public void setDtCd(String dtCd) {
		this.dtCd = dtCd;
	}

	public String getDtNm() {
		return this.dtNm;
	}

	public void setDtNm(String dtNm) {
		this.dtNm = dtNm;
	}

	public String getDtKr() {
		return this.dtKr;
	}

	public void setDtKr(String dtKr) {
		this.dtKr = dtKr;
	}

	public String getDtVn() {
		return this.dtVn;
	}

	public void setDtVn(String dtVn) {
		this.dtVn = dtVn;
	}

	public String getDtExp() {
		return this.dtExp;
	}

	public void setDtExp(String dtExp) {
		this.dtExp = dtExp;
	}

	public String getUpCd() {
		return this.upCd;
	}

	public void setUpCd(String upCd) {
		this.upCd = upCd;
	}

	public String getVal1() {
		return this.val1;
	}

	public void setVal1(String val1) {
		this.val1 = val1;
	}

	public String getVal1Nm() {
		return this.val1Nm;
	}

	public void setVal1Nm(String val1Nm) {
		this.val1Nm = val1Nm;
	}

	public String getVal2() {
		return this.val2;
	}

	public void setVal2(String val2) {
		this.val2 = val2;
	}

	public String getVal2Nm() {
		return this.val2Nm;
	}

	public void setVal2Nm(String val2Nm) {
		this.val2Nm = val2Nm;
	}

	public String getVal3() {
		return this.val3;
	}

	public void setVal3(String val3) {
		this.val3 = val3;
	}

	public String getVal3Nm() {
		return this.val3Nm;
	}

	public void setVal3Nm(String val3Nm) {
		this.val3Nm = val3Nm;
	}

	public String getVal4() {
		return this.val4;
	}

	public void setVal4(String val4) {
		this.val4 = val4;
	}

	public String getVal4Nm() {
		return this.val4Nm;
	}

	public void setVal4Nm(String val4Nm) {
		this.val4Nm = val4Nm;
	}

	public Integer getDtOrder() {
		return this.dtOrder;
	}

	public void setDtOrder(Integer dtOrder) {
		this.dtOrder = dtOrder;
	}

	public Character getUseYn() {
		return this.useYn;
	}

	public void setUseYn(Character useYn) {
		this.useYn = useYn;
	}

	public Character getDelYn() {
		return this.delYn;
	}

	public void setDelYn(Character delYn) {
		this.delYn = delYn;
	}

	public String getRegId() {
		return this.regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public Date getRegDt() {
		return this.regDt;
	}

	public void setRegDt(Date regDt) {
		this.regDt = regDt;
	}

	public String getChgId() {
		return this.chgId;
	}

	public void setChgId(String chgId) {
		this.chgId = chgId;
	}

	public Date getChgDt() {
		return this.chgDt;
	}

	public void setChgDt(Date chgDt) {
		this.chgDt = chgDt;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
