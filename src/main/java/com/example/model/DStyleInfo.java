package com.example.model;
// Generated Jul 9, 2019 10:29:51 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

/**
 * DStyleInfo generated by hbm2java
 */
public class DStyleInfo implements java.io.Serializable {

	private Integer sid;
	private String styleNo;
	private String mdCd;
	private String prjNm;
	private String ssver;
	private String partNm;
	private String standard;
	private String custRev;
	private String orderNum;
	private Integer packAmt;
	private String cav;
	private String bomType;
	private String tdsNo;
	private Character useYn;
	private char delYn;
	private String regId;
	private Date regDt;
	private String chgId;
	private Date chgDt;

	public DStyleInfo() {
	}

	public DStyleInfo(char delYn, Date regDt, Date chgDt) {
		this.delYn = delYn;
		this.regDt = regDt;
		this.chgDt = chgDt;
	}

	public DStyleInfo(String styleNo, String mdCd, String prjNm, String ssver, String partNm, String standard,
			String custRev, String orderNum, Integer packAmt, String cav, String bomType, String tdsNo, Character useYn,
			char delYn, String regId, Date regDt, String chgId, Date chgDt) {
		this.styleNo = styleNo;
		this.mdCd = mdCd;
		this.prjNm = prjNm;
		this.ssver = ssver;
		this.partNm = partNm;
		this.standard = standard;
		this.custRev = custRev;
		this.orderNum = orderNum;
		this.packAmt = packAmt;
		this.cav = cav;
		this.bomType = bomType;
		this.tdsNo = tdsNo;
		this.useYn = useYn;
		this.delYn = delYn;
		this.regId = regId;
		this.regDt = regDt;
		this.chgId = chgId;
		this.chgDt = chgDt;
	}

	public Integer getSid() {
		return this.sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getStyleNo() {
		return this.styleNo;
	}

	public void setStyleNo(String styleNo) {
		this.styleNo = styleNo;
	}

	public String getMdCd() {
		return this.mdCd;
	}

	public void setMdCd(String mdCd) {
		this.mdCd = mdCd;
	}

	public String getPrjNm() {
		return this.prjNm;
	}

	public void setPrjNm(String prjNm) {
		this.prjNm = prjNm;
	}

	public String getSsver() {
		return this.ssver;
	}

	public void setSsver(String ssver) {
		this.ssver = ssver;
	}

	public String getPartNm() {
		return this.partNm;
	}

	public void setPartNm(String partNm) {
		this.partNm = partNm;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getCustRev() {
		return this.custRev;
	}

	public void setCustRev(String custRev) {
		this.custRev = custRev;
	}

	public String getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getPackAmt() {
		return this.packAmt;
	}

	public void setPackAmt(Integer packAmt) {
		this.packAmt = packAmt;
	}

	public String getCav() {
		return this.cav;
	}

	public void setCav(String cav) {
		this.cav = cav;
	}

	public String getBomType() {
		return this.bomType;
	}

	public void setBomType(String bomType) {
		this.bomType = bomType;
	}

	public String getTdsNo() {
		return this.tdsNo;
	}

	public void setTdsNo(String tdsNo) {
		this.tdsNo = tdsNo;
	}

	public Character getUseYn() {
		return this.useYn;
	}

	public void setUseYn(Character useYn) {
		this.useYn = useYn;
	}

	public char getDelYn() {
		return this.delYn;
	}

	public void setDelYn(char delYn) {
		this.delYn = delYn;
	}

	public String getRegId() {
		return this.regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public Date getRegDt() {
		return this.regDt;
	}

	public void setRegDt(Date regDt) {
		this.regDt = regDt;
	}

	public String getChgId() {
		return this.chgId;
	}

	public void setChgId(String chgId) {
		this.chgId = chgId;
	}

	public Date getChgDt() {
		return this.chgDt;
	}

	public void setChgDt(Date chgDt) {
		this.chgDt = chgDt;
	}

}
